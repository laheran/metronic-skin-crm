<?php

namespace App\Providers;

use App\Models\Customer;
use Config;
use DateInterval;
use DateTime;
use DateTimeZone;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Schema;
use Validator;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Item;
use App\Models\Language;
use App\Models\File;
use App\Models\Preference;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\RoleUser;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Check boot method is loaded or not.
     *
     * @var boolean
     */
    public $isBooted;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('convert', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
        Blade::directive('convert_date', function ($value) {
            $middleware = str_replace('auth:', '', request()->route()->middleware()[1]);
            $preference = Preference::getAll()->pluck('value', 'field')->toArray();
            if ($middleware == 'customer') {
                $timezone = Customer::getTimezone();
            } else {
                $timezone = $preference['default_timezone'];
            }
            if (empty($timezone)) {
                $timezone = config('app.timezone');
            }

            $today = new DateTime($value, new DateTimeZone(config('app.timezone')));
            $today->setTimezone(new DateTimeZone($timezone));
            $value = $today->format('Y-m-d h:m:s');

            $date_format_type = $preference['date_format_type'];
            $separator = $preference['date_sepa'] == ',' ? ' ' : $preference['date_sepa'];
            $data = str_replace(['/', '.', ' ', '-', ','], $separator, $date_format_type);

            $data = explode($separator, $data);

            $first = $data[0];
            $second = $data[1];
            $third = $data[2];

            if ($first == 'yyyy' && $second == 'mm' && $third == 'dd') {
                $dateInfo = str_replace(['/', '.', ' ', '-', ','], $separator, $value);
                $datas = explode($separator, $dateInfo);
                $year = $datas[0];
                $month = $datas[1];
                $day = $datas[2];
                $value = $year . $separator . $month . $separator . $day;
            } elseif ($first == 'dd' && $second == 'mm' && $third == 'yyyy') {
                $dateInfo = str_replace(['/', '.', ' ', '-', ','], $separator, $value);
                $datas = explode($separator, $dateInfo);
                $year = $datas[0];
                $month = $datas[1];
                $day = $datas[2];
                $value = $day . $separator . $month . $separator . $year;
            } elseif ($first == 'mm' && $second == 'dd' && $third == 'yyyy') {
                $dateInfo = str_replace(['/', '.', ' ', '-', ','], $separator, $value);
                $datas = explode($separator, $dateInfo);
                $year = $datas[0];
                $month = $datas[1];
                $day = $datas[2];
                $value = $month . $separator . $day . $separator . $year;
            } elseif ($first == 'dd' && $second == 'M' && $third == 'yyyy') {
                $dateInfo = str_replace(['/', '.', ' ', '-', ','], $separator, $value);
                $datas = explode($separator, $dateInfo);
                $year = $datas[0];
                $month = $datas[1];
                $day = $datas[2];

                $dateObj = DateTime::createFromFormat('!m', $month);
                $monthName = $dateObj->format('M');

                $value = $day . $separator . $monthName . $separator . $year;
            } elseif ($first == 'yyyy' && $second == 'M' && $third == 'dd') {
                $dateInfo = str_replace(['/', '.', ' ', '-', ','], $separator, $value);
                $datas = explode($separator, $dateInfo);
                $year = $datas[0];
                $month = $datas[1];
                $day = $datas[2];

                $dateObj = DateTime::createFromFormat('!m', $month);
                $monthName = $dateObj->format('M');
                $value = $year . $separator . $monthName . $separator . $day;
            }
            return $value;
        });
        Blade::directive('convert_time', function ($date) {
            $middleware = str_replace('auth:', '', request()->route()->middleware()[1]);
            $preference = Preference::getAll()->pluck('value', 'field')->toArray();
            if ($middleware == 'customer') {
                $timezone = Customer::getTimezone();
            } else {
                $timezone = $preference['default_timezone'];
            }
            if (!$timezone) {
                $timezone = date_default_timezone_get();
            }
            $userTimezone = new DateTimeZone($timezone);
            $gmtTimezone = new DateTimeZone('GMT');
            dd($date);
            $myDateTime = new DateTime($date, $gmtTimezone);
            $offset = $userTimezone->getOffset($myDateTime);
            $myInterval = DateInterval::createFromDateString((string)$offset . 'seconds');
            $myDateTime->add($myInterval);
            $time = $myDateTime->format('h:i A');
            return $time;
        });


        Schema::defaultStringLength(191);
        if (! $this->app->runningInConsole() && env('APP_INSTALL') == true) {
            View::composer('*', function ($view) { 
                if (!$this->isBooted) {
                    $preference = Preference::getAll()->pluck('value', 'field')->toArray();
                    $currency   = Currency::getDefault($preference);
                    $languages  = Language::getAll()->where('status', 'Active');
                    if (mb_strlen($preference['company_name']) > 17) {
                        $preference['company_name'] = mb_substr($preference['company_name'], 0, 17).'...';
                    }
                    $data = [
                        'date_format_type'   => $preference['date_format_type'],
                        'row_per_page'       => $preference['row_per_page'],
                        'dflt_lang'          => $preference['dflt_lang'],
                        'company_name'       => $preference['company_name'],
                        'company_logo'       => !empty($preference['company_logo']) ? $preference['company_logo'] : '',
                        'company_street'     => $preference['company_street'],
                        'company_city'       => $preference['company_city'],
                        'company_state'      => $preference['company_state'],
                        'company_country_id' => $preference['company_country'],
                        'company_zipCode'    => $preference['company_zip_code'],
                        'currency_symbol'    => $currency->symbol,
                        'currency_name'      => $currency->names,
                        'languages'          => $languages,
                        'decimal_digits'     => $preference['decimal_digits'],
                        'thousand_separator' => $preference['thousand_separator'],
                        'symbol_position'    => $preference['symbol_position'],
                        'default_currency'   => $currency
                    ];
                    $json = \Cache::get('lanObject-' . config('app.locale'));
                    if (empty($json)) {
                        $json = file_get_contents(resource_path('lang/' . config('app.locale') . '.json'));
                        \Cache::put('lanObject-' . config('app.locale'), $json, 86400);
                        
                    }
                    $data['json'] = $json;
                    $data['company_country_name'] = Country::getCountry($data['company_country_id']);
                    $data['favicon']              = !empty($preference['company_icon']) ? $preference['company_icon'] : '';
                    $data['company_name']         = $preference['company_name'] ? $preference['company_name'] : '';
                    $view->with($data); 
                    $this->isBooted = true;
                }
            });
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
