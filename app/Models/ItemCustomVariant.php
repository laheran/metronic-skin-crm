<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemCustomVariant
 *
 * @property int $id
 * @property int $item_id item_id refers the items table id column
 * @property string $variant_title
 * @property string|null $variant_value
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant query()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant whereVariantTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemCustomVariant whereVariantValue($value)
 * @mixin \Eloquent
 */
class ItemCustomVariant extends Model
{
	public $timestamps = false;
}
