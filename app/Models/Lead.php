<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lead
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $email
 * @property string|null $street
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip_code
 * @property int|null $country_id
 * @property string|null $phone
 * @property string|null $website
 * @property string|null $company
 * @property string|null $description
 * @property int $lead_status_id
 * @property int $lead_source_id
 * @property int|null $assignee_id
 * @property int|null $user_id
 * @property string|null $last_contact
 * @property string|null $last_status_change
 * @property int|null $last_lead_status
 * @property string|null $date_converted
 * @property string|null $date_assigned
 * @property int $is_lost
 * @property int $is_public
 * @property string $created_at
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\LeadSource $leadSource
 * @property-read \App\Models\LeadStatus $leadStatus
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereAssigneeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDateAssigned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDateConverted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereIsLost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLastContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLastLeadStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLastStatusChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLeadSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereLeadStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereZipCode($value)
 * @mixin \Eloquent
 */
class Lead extends Model
{
	public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'assignee_id');
    }

    public function leadStatus()
    {
        return $this->belongsTo('App\Models\LeadStatus', 'lead_status_id');
    }

    public function leadSource()
    {
        return $this->belongsTo('App\Models\LeadSource', 'lead_source_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function generateMonthlyLeadData($from = null, $to = null) 
    {   
        $leadData = [];
        $finalData = [];
        $dataFilter = [];
        $data = [];
        
        $from = DbDateFormat($from);
        $to = DbDateFormat($to);
        $data = $this->where('is_lost', 0);
        $data->with(['leadStatus:id,name,color,status', 'leadSource:id,name,status']);
        $data->whereBetween('created_at', [$from, $to]);
        $data = $data->get()->toArray();

        for ($i=0; $i < count($data); $i++) { 
            if ($data[$i]['lead_status']['status'] == 'active' && $data[$i]['lead_source']['status'] == 'active' ) {
                $dataFilter[$i]['first_name'] = $data[$i]['first_name'];
                $dataFilter[$i]['last_name'] = $data[$i]['last_name'];
                $dataFilter[$i]['email'] = $data[$i]['email'];
                $dataFilter[$i]['date'] = date('M Y', strtotime($data[$i]['created_at']));
                $dataFilter[$i]['lead_status_name'] = $data[$i]['lead_status']['name'];
                $dataFilter[$i]['lead_status_color'] = $data[$i]['lead_status']['color'];
                $dataFilter[$i]['lead_source_name'] = $data[$i]['lead_source']['name'];
            }
        }
        for ($i=0; $i < count($dataFilter); $i++) { 
            if (array_key_exists($dataFilter[$i]['lead_status_name'], $leadData)) {
                $leadData[$dataFilter[$i]['lead_status_name']][$dataFilter[$i]['date']][] = $dataFilter[$i];
            } else {
                $leadData[$dataFilter[$i]['lead_status_name']][$dataFilter[$i]['date']][] = $dataFilter[$i];
            }
        }

        foreach ($leadData as $label => $data) {
            foreach ($data as $month => $value) {
                $finalData[$label][$month] = count($value);
            }
        }

        return $finalData;
    }
}
