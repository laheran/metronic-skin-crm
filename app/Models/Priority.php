<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\Priority
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @method static \Illuminate\Database\Eloquent\Builder|Priority newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Priority newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Priority query()
 * @method static \Illuminate\Database\Eloquent\Builder|Priority whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Priority whereName($value)
 * @mixin \Eloquent
 */
class Priority extends Model
{
	public $timestamps = false;

    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'priority');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket', 'priority_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-priorities');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-priorities', $data, 30 * 86400);
        }
        return $data;
    }
}
