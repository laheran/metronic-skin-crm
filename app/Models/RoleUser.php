<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\RoleUser
 *
 * @property int $user_id
 * @property int $role_id
 * @method static \Illuminate\Database\Eloquent\Builder|RoleUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleUser whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoleUser whereUserId($value)
 * @mixin \Eloquent
 */
class RoleUser extends Model
{
    public static function getAll()
    {
        $data = Cache::get('gb-role_users');
        if (empty($data)) {
            $data = DB::table('role_users')->get();
            Cache::put('gb-role_users', $data, 30 * 86400);
        }

        return $data;
    }
}
