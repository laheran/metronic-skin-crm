<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\TaskStatus
 *
 * @property int $id
 * @property string $name
 * @property int $status_order
 * @property string $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskStatus whereStatusOrder($value)
 * @mixin \Eloquent
 */
class TaskStatus extends Model
{
	public $timestamps = false;
    
    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'task_status_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-task_statues');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-task_statues', $data, 30 * 86400);
        }
        return $data;
    }
}
