<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StockMove
 *
 * @property int $id
 * @property int $item_id item_id refers items table id
 * @property int|null $transaction_type_id Id of sale_orders, purchase_orders, stock_adjustments, stock_transfers, items (initial stock)
 * @property string $transaction_type
 * @property int $location_id location_id refers locations table id
 * @property string $transaction_date
 * @property int|null $user_id user_id refers users table id
 * @property int|null $transaction_type_detail_id Id of sale_order_details, purchase_order_details, stock_adjustment_details, stock_transfers, items (initial stock)
 * @property string $reference
 * @property string|null $note
 * @property string $quantity
 * @property string $price
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\Location $location
 * @property-read \App\Models\SaleOrder|null $saleOrder
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereTransactionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereTransactionTypeDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereTransactionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMove whereUserId($value)
 * @mixin \Eloquent
 */
class StockMove extends Model
{
	public $timestamps = false;
    
    public function item()
    {
    	return $this->belongsTo("App\Models\Item", 'item_id');
  	}

  	public function location()
  	{
  		return $this->belongsTo("App\Models\Location");
  	}

  	public function saleOrder()
  	{
  		return $this->belongsTo("App\Models\SaleOrder", 'transaction_type_id');
  	}

	public function getItemQtyByLocationName($location_id, $item_id)
	{
		$qty = $this->where(['location_id' => $location_id, 'item_id' => $item_id])->sum('quantity');
	    if (empty($qty)) {
	        $qty = 0;
	    }
	    return $qty;
	}

}
