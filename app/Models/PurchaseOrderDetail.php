<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * App\Models\PurchaseOrderDetail
 *
 * @property int $id
 * @property int $purchase_order_id
 * @property int|null $item_id
 * @property string|null $description
 * @property string $item_name
 * @property string $discount
 * @property string $discount_type % or $
 * @property string|null $hsn
 * @property int $sorting_no
 * @property string $discount_amount
 * @property string $unit_price
 * @property string $quantity_ordered
 * @property string $quantity_received
 * @property-read \App\Models\Item|null $item
 * @property-read \App\Models\PurchaseOrder $purchaseOrder
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseTax[] $purchaseTaxes
 * @property-read int|null $purchase_taxes_count
 * @property-read \App\Models\PurchaseTax|null $totalTax
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereDiscountAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereDiscountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereHsn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereItemName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail wherePurchaseOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereQuantityOrdered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereQuantityReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereSortingNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderDetail whereUnitPrice($value)
 * @mixin \Eloquent
 */
class PurchaseOrderDetail extends Model
{
    public $timestamps = false;

    public function purchaseOrder()
    {
    	return $this->belongsTo('App\Models\PurchaseOrder', 'purchase_order_id');
    }

    public function item()
    {
    	return $this->belongsTo('App\Models\Item', 'item_id');
    }

    public function purchaseTaxes()
    {
    	return $this->hasMany('App\Models\PurchaseTax', 'purchase_order_detail_id');
    }

    public function totalTax()
    {
        return $this->hasOne('App\Models\PurchaseTax')
                ->selectRaw('purchase_order_detail_id,SUM(tax_amount) as total_tax')
                ->groupBy('purchase_order_detail_id');
    }
}
