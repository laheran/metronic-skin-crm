<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\PurchaseOrder;

/**
 * App\Models\Expense
 *
 * @property int $id
 * @property int|null $transaction_id transaction_id refers transactions tables' id;
 * @property int|null $user_id user_id refers users tables' id;
 * @property int $transaction_reference_id transaction_reference_id refers transaction_references tables'  id;
 * @property int $income_expense_category_id income_expense_category_id refers income_expense_categories tables' id;
 * @property int $currency_id currency_id refers currencies tables' id;
 * @property int|null $payment_method_id payment_method_id refers payment_methods tables' id;
 * @property string $amount
 * @property string|null $note
 * @property string $transaction_date
 * @property string $created_at
 * @property-read \App\Models\IncomeExpenseCategory $incomeExpenseCategory
 * @property-read \App\Models\Currency $incomeExpenseCurrency
 * @property-read \App\Models\PaymentMethod|null $paymentMethod
 * @property-read \App\Models\Transaction|null $transaction
 * @property-read \App\Models\TransactionReference $transactionReference
 * @method static \Illuminate\Database\Eloquent\Builder|Expense newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Expense newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Expense query()
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereIncomeExpenseCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereTransactionReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Expense whereUserId($value)
 * @mixin \Eloquent
 */
class Expense extends Model
{
    public $timestamps = false;

    // Relation
    public function transaction()
    {
       return $this->belongsTo("App\Models\Transaction", 'transaction_id');
    }

    public function transactionReference()
    {
       return $this->belongsTo("App\Models\TransactionReference",'transaction_reference_id');
    }

    public function incomeExpenseCategory() 
    {
      return $this->belongsTo("App\Models\IncomeExpenseCategory", 'income_expense_category_id');
    }

    public function incomeExpenseCurrency() 
    {
      return $this->belongsTo("App\Models\Currency", 'currency_id');
    }

    public function paymentMethod() 
    {
      return $this->belongsTo("App\Models\PaymentMethod", 'payment_method_id');
    }
    // End Relation
    
    public function getAllExpensesById($from, $to, $account_no)
    {
        $from = DbDateFormat($from);
        $to   = DbDateFormat($to);
        if ($account_no != 'all') {
          $data = DB::table('transactions')
                ->leftJoin('accounts', 'accounts.id', '=', 'transactions.account_no' )
                ->where('transactions.transaction_type_name', 'expense')
                ->where('transactions.account_id', $account_no)
                ->whereDate('transactions.transaction_date', '>=', $from)
                ->whereDate('transactions.transaction_date', '<=', $to)
                ->select('transactions.*', 'accounts.name', 'accounts.account_number as acc_no');
        } else {
           $data = DB::table('transactions')
                ->leftJoin('accounts', 'accounts.id', '=', 'transactions.account_no' )
                ->where('transaction_type_name', 'expense')
                ->whereDate('transactions.transaction_date', '>=', $from)
                ->whereDate('transactions.transaction_date', '<=', $to)
                ->select('transactions.*', 'accounts.name', 'accounts.account_number as acc_no');
        }

        return $data;
    }
    
    public function getExpenseStat($from, $to, $currency = null)
    {
      error_reporting(E_ALL - E_NOTICE);

      $expenseStat = [];
      $preference = Preference::getAll()->pluck('value', 'field')->toArray();
      $from = DbDateFormat($from);
      $to   = DbDateFormat($to);
      $currency = isset($currency) && !empty($currency) ? (int) $currency : (int) $preference['dflt_currency_id'];
      $data = DB::select(DB::raw("SELECT ec.id, ec.name as expense_name, sum(e.amount) as expense, DATE_FORMAT(e.transaction_date, '%b %Y') as edate, e.transaction_date as normalDate FROM income_expense_categories as ec LEFT JOIN `expenses` as e on(e.income_expense_category_id = ec.id) WHERE ec.category_type = 'expense' AND e.currency_id = '$currency' AND e.transaction_date > '$from' AND e.transaction_date <= '$to' GROUP BY ec.id, edate ORDER BY e.transaction_date"));

      foreach ($data as $key => $value) {
        $expenseStat[$value->expense_name][$value->edate] += $value->expense;
      }
      if (!empty($expenseStat)) {
        return ['expenses' => $expenseStat];
      }
    }

    public function getGenerelExpenses($from = null, $to = null, $currency = null)
    {
      error_reporting(E_ALL - E_NOTICE);

      $expenseStat = [];
      $preference = Preference::getAll()->pluck('value', 'field')->toArray();
      $currency = isset($currency) && !empty($currency) ? (int) $currency : (int) $preference['dflt_currency_id'];
      if (!empty($from) && !empty($to)) {
        $extra = " AND e.transaction_date > '". $from ."' AND e.transaction_date <= '". $to ."'";
      }
      $data = DB::select(DB::raw("SELECT ec.id, ec.name as expense_name, sum(e.amount) as expense, DATE_FORMAT(e.transaction_date, '%b %Y') as edate, e.transaction_date as normalDate FROM income_expense_categories as ec LEFT JOIN `expenses` as e on(e.income_expense_category_id = ec.id) WHERE ec.category_type = 'expense' AND e.currency_id = " . $currency . $extra . "  GROUP BY ec.id, edate ORDER BY e.transaction_date"));

      foreach ($data as $key => $value) {
        $expenseStat[$value->expense_name][$value->edate] += $value->expense;
      }
      if (!empty($expenseStat)) {
        return ['expenses' => $expenseStat];
      } else {
        return ['expenses' => []];
      }
    }

    public function getPurchaseExpenses($from = null, $to = null, $currency = null)
    {
      $data = [];
      $preference = Preference::getAll()->pluck('value', 'field')->toArray();
      
      $data = PurchaseOrder::with(['currency:id,name,symbol']);

      if (!empty($from) && !empty($to)) {
        $data->where('order_date', '>=', $from)->where('order_date', '<=', $to); 
      }
      if (!empty($currency)) {
        $data->where('currency_id', '=', (int) $currency);
      }
      return $data;
    }
}
