<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Supplier
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $street
 * @property string|null $contact
 * @property string|null $tax_id
 * @property int $currency_id currency_id refers currencies table id
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zipcode
 * @property int|null $country_id
 * @property int $is_active 1 for active; \n 0 for inactive.
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier query()
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereTaxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereZipcode($value)
 * @mixin \Eloquent
 */
class Supplier extends Model
{
    public $timestamps = false;

    public function currency()
    {
        return $this->belongsTo("App\Models\Currency",'currency_id');
    }
    public function country()
    {
    	return $this->belongsTo("App\Models\Country", 'country_id');
    }
    public function purchaseOrders()
    {
        return $this->hasMany('App\Models\PurchaseOrder', 'supplier_id');
    }
}
