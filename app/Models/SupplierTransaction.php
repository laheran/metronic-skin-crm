<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Helpers;
use Session;

/**
 * App\Models\SupplierTransaction
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $transaction_reference_id
 * @property int $currency_id
 * @property int $supplier_id
 * @property int $purchase_order_id
 * @property int|null $payment_method_id
 * @property string|null $transaction_date
 * @property string|null $amount
 * @property string $exchange_rate
 * @property string|null $created_at
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\PaymentMethod|null $paymentMethod
 * @property-read \App\Models\PurchaseOrder $purchaseOrder
 * @property-read \App\Models\Supplier $supplier
 * @property-read \App\Models\Transaction $transaction
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereExchangeRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction wherePurchaseOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereTransactionReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierTransaction whereUserId($value)
 * @mixin \Eloquent
 */
class SupplierTransaction extends Model
{
    public $timestamps = false;

    public function paymentMethod()
    {
       return $this->belongsTo("App\Models\PaymentMethod"); 
    }

    public function currency()
    {
       return $this->belongsTo("App\Models\Currency"); 
    }

    public function supplier()
    {
       return $this->belongsTo("App\Models\Supplier"); 
    }

    public function purchaseOrder()
    {
       return $this->belongsTo('App\Models\PurchaseOrder', 'purchase_order_id'); 
    }

    public function transaction()
    {
        return $this->belongsTo("App\Models\Transaction", 'transaction_reference_id', 'transaction_reference_id');
    }

    public function getAll($supplier = null, $method = null, $currency = null, $from = null, $to = null) 
    {
        $purchasesPayment = $this->select('supplier_transactions.id as st_id', 'supplier_transactions.supplier_id', 'supplier_transactions.payment_method_id', 'supplier_transactions.currency_id', 'supplier_transactions.purchase_order_id', 'supplier_transactions.amount', 'supplier_transactions.transaction_date')->with(['supplier:id,name', 'paymentMethod:id,name', 'currency:id,name', 'purchaseOrder:id,reference']);

        if (!empty($from)) {
             $purchasesPayment->where('transaction_date', '>=', DbDateFormat($from));
        }
        if (!empty($to)) {
             $purchasesPayment->where('transaction_date', '<=', DbDateFormat($to));
        }
        if (!empty($supplier)) {
             $purchasesPayment->where('supplier_id', '=', $supplier);
        }
        if (!empty($currency)) {
             $purchasesPayment->where('currency_id', '=', $currency);
        }
        if (!empty($method)) {
             $purchasesPayment->where('payment_method_id', '=', $method);
        }
        if (Helpers::has_permission(Auth::user()->id, 'own_purchase_payment') && !Helpers::has_permission(Auth::user()->id, 'manage_purch_payment')) {
            $id = Auth::user()->id;
            $purchasesPayment->where('user_id', $id);
        }

        return $purchasesPayment;
    }
}
