<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PurchaseType
 *
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseType query()
 * @mixin \Eloquent
 */
class PurchaseType extends Model
{
    public $timestamps = false;
}
