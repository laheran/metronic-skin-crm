<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionFile
 *
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionFile query()
 * @mixin \Eloquent
 */
class TransactionFile extends Model
{
	
}
