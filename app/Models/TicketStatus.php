<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

/**
 * App\Models\TicketStatus
 *
 * @property int $id
 * @property string|null $name
 * @property int $is_default 1 for default; 0 otherwise
 * @property string $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketStatus whereName($value)
 * @mixin \Eloquent
 */
class TicketStatus extends Model
{
	public $timestamps = false;
	
	public function tickets()
	{
        return $this->hasMany('App\Models\Ticket', 'ticket_status_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-ticketStatus');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-ticketStatus', $data, 30 * 86400);
        }

        return $data;
    }
    
}
