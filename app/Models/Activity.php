<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Activity
 *
 * @property int $id
 * @property int $object_id
 * @property string|null $object_type
 * @property int|null $user_id
 * @property int|null $customer_id
 * @property string|null $params
 * @property string|null $description
 * @property string $created_at
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Activity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Activity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Activity query()
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Activity whereUserId($value)
 * @mixin \Eloquent
 */
class Activity extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function store($objectType = null, $objectId = null, $authType = null, $authId = null, $description = null, $options = []) 
    {
      	$id = '';
		if (empty($objectType) || empty($objectId) || empty($authType) || empty($authId) ||  empty($description) ) {
			return $id;
		}
		$data                     = new Activity();
		$data->object_id          = $objectId;
		$data->object_type        = $objectType;
		if ($authType == 'user') {
			$data->user_id = $authId;
		} else {
			$data->customer_id = $authId;
		}
		$data->description          = $description;
		if ($data->save()) {
			$id = $data->id;
		}

		return $id;
    }
}
