<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PurchasePrice
 *
 * @property int $id
 * @property int $item_id
 * @property int $currency_id
 * @property string|null $price
 * @property-read \App\Models\Item $item
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchasePrice wherePrice($value)
 * @mixin \Eloquent
 */
class PurchasePrice extends Model
{
    public $timestamps  = false;
    protected $fillable = ['item_id', 'price'];

    public function item()
    {
      return $this->belongsTo("App\Models\Item", 'item_id');
    }
}
