<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transfer
 *
 * @property int $id
 * @property int $from_account_id
 * @property int $to_account_id
 * @property int|null $user_id
 * @property int $from_currency_id
 * @property int $to_currency_id
 * @property string $transaction_date
 * @property int $transaction_reference_id
 * @property string|null $description
 * @property string $amount
 * @property string $fee
 * @property string|null $exchange_rate
 * @property string $incoming_amount
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\Account $fromBank
 * @property-read \App\Models\Account $toBank
 * @property-read \App\Models\TransactionReference $transactionReference
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereExchangeRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereFromAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereFromCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereIncomingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereToAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereToCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereTransactionReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereUserId($value)
 * @mixin \Eloquent
 */
class Transfer extends Model
{
    public $timestamps = false;

    public function fromBank()
    {
       return $this->belongsTo("App\Models\Account", 'from_account_id');
    }
    public function toBank()
    {
       return $this->belongsTo("App\Models\Account", 'to_account_id');
    }

    public function currency()
    {
    	 return $this->belongsTo("App\Models\Currency", 'to_currency_id');
    }

    public function transactionReference()
    {
         return $this->belongsTo("App\Models\TransactionReference");
    }
}
