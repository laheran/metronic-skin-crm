<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Rules\{
    CheckValidEmail
};
use Validator;
use Cache;

/**
 * App\Models\EmailConfiguration
 *
 * @property int $id
 * @property string $protocol
 * @property string $encryption
 * @property string $smtp_host
 * @property string $smtp_port
 * @property string $smtp_email
 * @property string $smtp_username
 * @property string $smtp_password
 * @property string $from_address
 * @property string $from_name
 * @property int $status 1= varified, 0= unverified
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereEncryption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereFromAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereProtocol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereSmtpEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereSmtpHost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereSmtpPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereSmtpPort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereSmtpUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailConfiguration whereStatus($value)
 * @mixin \Eloquent
 */
class EmailConfiguration extends Model
{
    public $timestamps = false;
    protected function validation($data = [])
    {
        $validator = Validator::make($data, [
            'type' => 'required|in:smtp,sendmail',
            'encryption' => 'required',
            'smtp_host' => 'required',
            'smtp_port' => 'required',
            'smtp_email' => ['required', 'email', new CheckValidEmail],
            'from_address' => ['required', 'email', new CheckValidEmail],
            'from_name' => ['required', 'email', new CheckValidEmail],
            'smtp_username' => ['required', 'email', new CheckValidEmail],
            'smtp_password' => 'required'
        ]);
        return $validator;
    }
    public static function getAll()
    {
        $data = Cache::get('gb-email_configurations');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-email_configurations', $data, 30 * 86400);
        }
        return $data;
    }
    public function store($request = [])
    {
        if (parent::updateOrInsert(['id' => 1], $request)) {
            Cache::forget('gb-email_configurations');
            return true;
        }
        return false;
    }

}
