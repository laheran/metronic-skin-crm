<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StockAdjustment
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $location_id
 * @property string $transaction_type
 * @property string $total_quantity
 * @property string|null $note
 * @property string|null $transaction_date
 * @property-read \App\Models\Location $location
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockAdjustmentDetail[] $stockAdjustmentDetails
 * @property-read int|null $stock_adjustment_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereTotalQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereTransactionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustment whereUserId($value)
 * @mixin \Eloquent
 */
class StockAdjustment extends Model
{
	public $timestamps = false;

	public function location()
	{
		return $this->belongsTo("App\Models\Location");
	}

	public function stockAdjustmentDetails()
	{
		return $this->hasMany("App\Models\StockAdjustmentDetail");
	}
}