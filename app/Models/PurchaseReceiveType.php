<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\PurchaseReceiveType
 *
 * @property int $id
 * @property string $receive_type
 * @property int $is_default 1 for default.
 * 0 for otherwise.
 * @property-read \App\Models\PurchaseOrder|null $purchaseOrder
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseReceiveType whereReceiveType($value)
 * @mixin \Eloquent
 */
class PurchaseReceiveType extends Model
{
    public $timestamps = false;

    public function purchaseOrder()
    {
    	return $this->hasOne('App\Models\PurchaseOrder', 'purchase_receive_type_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-purchase_receive_types');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-purchase_receive_types', $data, 30 * 86400);
        }
        return $data;
    }
}
