<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReceivedOrderDetail
 *
 * @property int $id
 * @property int $purchase_order_id
 * @property int $purchase_order_detail_id
 * @property int $received_order_id
 * @property int|null $item_id
 * @property string $item_name
 * @property string $unit_price
 * @property string $quantity
 * @property-read \App\Models\PurchaseOrder $purchaseOrder
 * @property-read \App\Models\ReceivedOrder $receivedOrder
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereItemName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail wherePurchaseOrderDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail wherePurchaseOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereReceivedOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReceivedOrderDetail whereUnitPrice($value)
 * @mixin \Eloquent
 */
class ReceivedOrderDetail extends Model
{
    public $timestamps = false;
    public function receivedOrder()
    {
    	return $this->belongsTo('App\Models\ReceivedOrder', 'received_order_id');
    }

    public function purchaseOrder()
    {
    	return $this->belongsTo('App\Models\PurchaseOrder', 'purchase_order_id');
    }
}
