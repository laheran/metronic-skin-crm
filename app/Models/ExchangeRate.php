<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExchangeRate
 *
 * @property int $id
 * @property int $currency_id
 * @property string $rate
 * @property string $date
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExchangeRate whereRate($value)
 * @mixin \Eloquent
 */
class ExchangeRate extends Model
{
    public $timestamps = false;
}
