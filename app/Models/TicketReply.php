<?php

namespace App\Models;

use App\Http\Start\Helpers;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;

/**
 * App\Models\TicketReply
 *
 * @property int $id
 * @property int $ticket_id ticket_id refers tickets table id
 * @property int|null $user_id user_id refers users table id in other word (admin id who replied the current one)
 * @property int|null $customer_id customer_id refers customers table id
 * @property string|null $date
 * @property string|null $message
 * @property int $has_attachment 0 for no attachment;
 * 1 means this reply has attachment
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\Ticket $ticket
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply query()
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereHasAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereTicketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TicketReply whereUserId($value)
 * @mixin \Eloquent
 */
class TicketReply extends Model
{
  	public function ticket()
	{
	    return $this->belongsTo('App\Models\Ticket','ticket_id');
	}

	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}

	public function customer()
	{
	    return $this->belongsTo('App\Models\Customer','customer_id');
	}
}
