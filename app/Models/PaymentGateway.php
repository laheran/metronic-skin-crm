<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\PaymentGateway
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $site
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentGateway whereValue($value)
 * @mixin \Eloquent
 */
class PaymentGateway extends Model
{
    public $timestamps = false;
    protected $fillable = ['value'];

    public static function getAll()
    {
        $data = Cache::get('gb-payment_gateways');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-payment_gateways', $data, 30 * 86400);
        }
        return $data;
    }
}
