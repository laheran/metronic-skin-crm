<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\Location
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $delivery_address
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $contact
 * @property int $is_active 1 = 'Active'\n 0 = 'Inactive'
 * @property int $is_default 1 = 'Yes'\n 0 = 'No'
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SaleOrder[] $saleOrders
 * @property-read int|null $sale_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockMove[] $stockMoves
 * @property-read int|null $stock_moves_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockTransfer[] $stockTransferDestinatin
 * @property-read int|null $stock_transfer_destinatin_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockTransfer[] $stockTransferSource
 * @property-read int|null $stock_transfer_source_count
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Location extends Model
{
    public function saleOrders()
    {
        return $this->hasMany('App\Models\SaleOrder', 'location_id');
    }
    
    public function purchaseOrders()
    {
    	return $this->hasMany('App\Models\PurchaseOrder', 'location_id');
    }

    public function stockTransferDestinatin()
    {
        return $this->hasMany('App\Models\StockTransfer', 'destination_location_id');
    }

    public function stockTransferSource()
    {
        return $this->hasMany('App\Models\StockTransfer', 'source_location_id');
    }

    public function stockMoves()
    {
        return $this->hasMany('App\Models\StockMove');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-locations');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-locations', $data, 30 * 86400);
        }
        return $data;
    }

    public static function getLocationName($id)
    {
        $location = Location::where('id', $id)->first();
        return $location->name;
    }
}
