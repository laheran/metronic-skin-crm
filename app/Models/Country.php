<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property-read \App\Models\CustomerBranch|null $customerBranch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read int|null $leads_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Supplier[] $suppliers
 * @property-read int|null $suppliers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereName($value)
 * @mixin \Eloquent
 */
class Country extends Model
{
    public $timestamps = false;

    public function leads()
    {
        return $this->hasMany('App\Models\Lead', 'country_id');
    }

    public function customerBranch()
    {
        return $this->hasOne('App\Models\CustomerBranch', 'billing_country_id');
    }

    public function suppliers()
    {
        return $this->hasMany("App\Models\Supplier", 'country_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-countries');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-countries', $data, 30 * 86400);
        }
        return $data;
    }

    public static function getCountry($id)
    {
        $country = self::getAll()->where('id', $id)->first();
        return $country->name;
    }
}
