<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;

/**
 * App\Models\StockCategory
 *
 * @property int $id
 * @property string $name
 * @property int $item_unit_id
 * @property int $is_active 1 for active;\n 0 for otherwise
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Item|null $item
 * @property-read \App\Models\ItemUnit $itemUnit
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereItemUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StockCategory extends Model
{
    public function itemUnit()
    {
        return $this->belongsTo('App\Models\ItemUnit','item_unit_id');
    }

    public function item()
    {
      return $this->hasOne('App\Models\Item','stock_category_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-stock_categories');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-stock_categories', $data, 30 * 86400);
        }
        return $data;
    }
}
