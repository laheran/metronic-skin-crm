<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;
use DB;

/**
 * App\Models\PaymentMethod
 *
 * @property int $id
 * @property string $name
 * @property string|null $mode
 * @property string|null $client_id
 * @property string|null $consumer_key
 * @property string|null $consumer_secret
 * @property string|null $approve
 * @property int $is_default 0 for not default;
 * 1 for default
 * @property int $is_active 0 for inactive;\n1 for active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerTransaction[] $customerTransactions
 * @property-read int|null $customer_transactions_count
 * @property-read \App\Models\Expense|null $expense
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereApprove($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereConsumerKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereConsumerSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereName($value)
 * @mixin \Eloquent
 */
class PaymentMethod extends Model
{
    public $timestamps = false;

    public function expense() 
    {
      return $this->hasOne("App\Models\Expense", 'payment_method_id');
    }

    public function customerTransactions()
    {
        return $this->hasMany('App\Models\CustomerTransaction', 'payment_method_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'payment_method_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-payment_methods');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-payment_methods', $data, 30 * 86400);
        }
        return $data;
    }

    public static function getPaymentMethodName($id)
    {
        $term = \DB::table('payment_methods')
            ->where('id', $id)
            ->select('name')
            ->first();
        return $term->name;
    }
}
