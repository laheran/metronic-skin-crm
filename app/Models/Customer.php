<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;
use Auth;


/**
 * App\Models\Customer
 *
 * @property int $id
 * @property string $name
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $email
 * @property string $password
 * @property string|null $phone
 * @property string|null $tax_id
 * @property int $currency_id currency_id refers to currencies tbale id.
 * @property string|null $customer_type
 * @property string|null $timezone
 * @property string $remember_token
 * @property int $is_active 1 for active;\n0 for inactive.
 * @property int $is_verified 1 for vairified;0 for not varified.
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Activity|null $activity
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\CustomerBranch|null $customerBranch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read int|null $projects_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCustomerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTaxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Customer extends Authenticatable
{
	protected $table = "customers";
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','email','is_activated','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function customerBranch()
    {
        return $this->hasOne("App\Models\CustomerBranch", 'customer_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency','currency_id');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'customer_id');
    }

    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket', 'customer_id');
    }

    public function activity()
    {
        return $this->hasOne('App\Models\Activity', 'customer_id');
    }

    public static function getTimezone()
    {
        $loggedCustomer = Auth::guard('customer')->user()->id;
        $data = Cache::get('gb-dflt_timezone_customer'.$loggedCustomer);
        if (empty($data)) {
            $data = parent::find($loggedCustomer)->timezone;
            Cache::put('gb-dflt_timezone_customer'.$loggedCustomer, $data, 30 * 86400);
        }
        return $data;
    }

}
