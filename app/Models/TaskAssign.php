<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskAssign
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $task_id
 * @property int $assigned_by
 * @property int $is_assigned_by_customer 0 for not;
 * 1 for yes;
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign whereAssignedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign whereIsAssignedByCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssign whereUserId($value)
 * @mixin \Eloquent
 */
class TaskAssign extends Model
{
	public $timestamps = false;
    
    public function task()
    {
        return $this->belongsTo('App\Models\Task');
    }
}
