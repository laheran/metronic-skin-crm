<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncomeExpenseCategory
 *
 * @property int $id
 * @property string $name
 * @property string $category_type
 * @property-read \App\Models\Deposit|null $deposit
 * @property-read \App\Models\Expense|null $expense
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory whereCategoryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeExpenseCategory whereName($value)
 * @mixin \Eloquent
 */
class IncomeExpenseCategory extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'category_type'];

    public function generalLedgers()
    {
        return $this->hasMany("App\Models\GeneralLedger", 'gl_account_id');
    }

    public function expense() 
    {
      return $this->hasOne("App\Models\Expense", 'income_expense_category_id');
    }
    
    public function deposit() 
    {
      return $this->hasOne("App\Models\Deposit", 'income_expense_category_id');
    }
}
