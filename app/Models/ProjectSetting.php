<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProjectSetting
 *
 * @property int $id
 * @property int $project_id
 * @property string|null $setting_label
 * @property string|null $setting_value ('on', 'off')
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting whereSettingLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectSetting whereSettingValue($value)
 * @mixin \Eloquent
 */
class ProjectSetting extends Model
{
    public $timestamps = false;
}