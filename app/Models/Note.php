<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Note
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $related_to_id
 * @property string|null $related_to_type
 * @property string $subject
 * @property string|null $content
 * @property string $created_at
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Note newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note query()
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereRelatedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereRelatedToType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereUserId($value)
 * @mixin \Eloquent
 */
class Note extends Model
{
    protected $fillable = ['subject', 'content'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getAllNoteByCustomer($from, $to, $id)
    {
        $noteData = Note::where(['related_to_id' => $id, 'related_to_type' => 'customer']);
        if (!empty($from)) {
            $noteData = $noteData->whereDate('created_at', '>=', DbDateTimeFormat($from))
                                 ->whereDate('created_at', '<=', DbDateTimeFormat($to))->orderBy('id', 'DESC');
        }

    	return $noteData;
    }

    public function getAllNoteByCustomerCSV($from, $to, $customer_id)
    {
        $data  = Note::where(['related_to_id'=>$customer_id, 'related_to_type' => 'customer']);
        if ($from && $to) {
            $data=null;
            $from = DbDateFormat($from);
            $to   = DbDateFormat($to);
            $data  = Note::where(['related_to_id' => $customer_id, 'related_to_type' => 'customer'])
            ->whereDate('created_at', '>=', DbDateTimeFormat($from))
            ->whereDate('created_at', '<=', DbDateTimeFormat($to))->orderBy('id', 'DESC');
        }

        return $data;
    }
}
