<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Milestone
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property string|null $description
 * @property int|null $is_visible_to_customer 1=yes, 0=no
 * @property string|null $due_date
 * @property int|null $order
 * @property string|null $color
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone query()
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereIsVisibleToCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Milestone whereProjectId($value)
 * @mixin \Eloquent
 */
class Milestone extends Model
{
    public $timestamps = false;
}
