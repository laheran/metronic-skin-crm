<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChecklistItem
 *
 * @property int $id
 * @property string $title
 * @property int|null $task_id task_id refers the task table's id column.
 * @property int $is_checked 1, 0 and 0 as default;\n0 for unchecked;\n1 for checked;
 * @property string|null $created_at
 * @property string|null $checked_at
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereIsChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChecklistItem whereTitle($value)
 * @mixin \Eloquent
 */
class ChecklistItem extends Model
{
    public $timestamps = false;
}
