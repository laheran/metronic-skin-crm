<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SalePrice
 *
 * @property int $id
 * @property int $item_id
 * @property int $sale_type_id
 * @property int $currency_id
 * @property string $price
 * @property-read \App\Models\Item $item
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalePrice whereSaleTypeId($value)
 * @mixin \Eloquent
 */
class SalePrice extends Model
{
	public $timestamps  = false;
	protected $fillable = ['item_id', 'sales_type_id', 'curr_abrev', 'price'];

    public function item(){
      return $this->belongsTo("App\Models\Item");
  	}

}
