<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * App\Models\Preference
 *
 * @property int $id
 * @property string $category
 * @property string $field
 * @property string $value
 * @method static \Illuminate\Database\Eloquent\Builder|Preference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Preference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Preference query()
 * @method static \Illuminate\Database\Eloquent\Builder|Preference whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preference whereField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preference whereValue($value)
 * @mixin \Eloquent
 */
class Preference extends Model
{
    public $timestamps = false;
    protected $fillable = ['category', 'field', 'value'];

    public static function getAll()
    {
        $data = Cache::get('gb-preferences');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-preferences', $data, 30 * 86400);
        }

        return $data;
    }
}
