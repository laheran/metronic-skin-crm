<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\SaleType
 *
 * @property int $id
 * @property string $sale_type
 * @property int $is_tax_included
 * @property int $is_default
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType query()
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType whereIsTaxIncluded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SaleType whereSaleType($value)
 * @mixin \Eloquent
 */
class SaleType extends Model
{
    public $timestamps = false;

    public static function getAll()
    {
        $data = Cache::get('gb-sale_types');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-sale_types', $data, 30 * 86400);
        }
        return $data;
    }
}
