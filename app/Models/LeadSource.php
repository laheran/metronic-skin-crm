<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LeadSource
 *
 * @property int $id
 * @property string $name
 * @property string $status ('active','inactive')
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read int|null $leads_count
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource query()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadSource whereStatus($value)
 * @mixin \Eloquent
 */
class LeadSource extends Model
{
	public $timestamps = false;

    public function leads()
    {
        return $this->hasMany('App\Models\Lead', 'lead_source_id');
    }
}
