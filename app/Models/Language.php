<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;  

/**
 * App\Models\Language
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property string|null $flag
 * @property string $status 'Active', 'Inactive
 * @property int $is_default
 * @property string $direction ('ltr', 'rtl') ltr = left-to-right directionrtl = right-to-left direction
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmailTemplate[] $emailTemplateDetails
 * @property-read int|null $email_template_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereStatus($value)
 * @mixin \Eloquent
 */
class Language extends Model
{
    public $timestamps = false;

    public function emailTemplateDetails()
    {
        return $this->hasMany('App\Models\EmailTemplate', 'language_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-languages');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-languages', $data, 30 * 86400);
        }
        return $data;
    }
}
