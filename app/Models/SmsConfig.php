<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SmsConfig
 *
 * @property int $id
 * @property string $type
 * @property string $status
 * @property string $key
 * @property string $secretkey
 * @property string $default
 * @property string $default_number
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereDefaultNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereSecretkey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfig whereType($value)
 * @mixin \Eloquent
 */
class SmsConfig extends Model
{
    protected $table = 'sms_config';
    public $timestamps = false;
}
