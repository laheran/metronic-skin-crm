<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GeneralLedgerTransaction
 *
 * @property int $id
 * @property int $reference_id
 * @property int $reference_type
 * @property int|null $user_id
 * @property int $gl_account_id
 * @property int $currency_id
 * @property string $exchange_rate
 * @property string $amount
 * @property string|null $comment
 * @property int $is_reversed 1 for yes, 0 for no
 * @property string $transaction_date
 * @property string $created_at
 * @property-read \App\Models\IncomeExpenseCategory|null $incomeExpenseCategory
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereExchangeRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereGlAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereIsReversed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereReferenceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereTransactionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeneralLedgerTransaction whereUserId($value)
 * @mixin \Eloquent
 */
class GeneralLedgerTransaction extends Model
{
    public $timestamps = false;

    public function reference()
    {
        return $this->belongsTo("App\Models\Reference", 'reference_id');
    }

    public function incomeExpenseCategory()
    {
        return $this->belongsTo("App\Models\IncomeExpenseCategory", 'gl_account_id');
    }
}
