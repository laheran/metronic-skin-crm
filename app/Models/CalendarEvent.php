<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CalendarEvent
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string $start_date
 * @property string|null $end_date
 * @property int|null $notification_start notification_start, it means how many days/hour/minutes/weeks before it will remind user about the event.
 * @property int|null $notification_repeat_every notification_repeat_every, it means what is the unit of 'notification_start' field.
 * i.e. : minutes, hours, days, weeks
 * @property string|null $event_color
 * @property int|null $is_public
 * @property int|null $created_by created by refers the users table id;
 * it is the id who was logged in at the creation time of the event.
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereEventColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereNotificationRepeatEvery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereNotificationStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarEvent whereTitle($value)
 * @mixin \Eloquent
 */
class CalendarEvent extends Model
{
    public $timestamps = false;
}
