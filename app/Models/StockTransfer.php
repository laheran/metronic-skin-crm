<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StockTransfer
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $source_location_id
 * @property int $destination_location_id
 * @property string|null $note
 * @property string $quantity
 * @property string $transfer_date
 * @property-read \App\Models\Location $destinationLocation
 * @property-read \App\Models\Location $sourceLocation
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereDestinationLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereSourceLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereTransferDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockTransfer whereUserId($value)
 * @mixin \Eloquent
 */
class StockTransfer extends Model
{
    public $timestamps = false;

    public function sourceLocation() {
	    return $this->belongsTo('App\Models\Location', 'source_location_id');
	}

	public function destinationLocation() {
	    return $this->belongsTo('App\Models\Location', 'destination_location_id');
	}
}
