<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\EmailTemplate
 *
 * @property int $id
 * @property int $template_id
 * @property string $subject
 * @property string $body
 * @property string $language_short_name
 * @property string $template_type 'email', 'sms'
 * @property int $language_id
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereLanguageShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereTemplateType($value)
 * @mixin \Eloquent
 */
class EmailTemplate extends Model
{
	public $timestamps = false;

    public function language()
    {
        return $this->belongsTo('App\Models\Language', 'language_id');
    }

    public static function getAll()
    {
        $data = Cache::get('gb-email_template');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-email_template', $data, 30 * 86400);
        }
        return $data;
    }
}
