<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerBranch
 *
 * @property int $id
 * @property int $customer_id
 * @property string|null $name
 * @property string|null $contact
 * @property string|null $billing_street
 * @property string|null $billing_city
 * @property string|null $billing_state
 * @property string|null $billing_zip_code
 * @property int|null $billing_country_id
 * @property string|null $shipping_street
 * @property string|null $shipping_city
 * @property string|null $shipping_state
 * @property string|null $shipping_zip_code
 * @property int|null $shipping_country_id
 * @property-read \App\Models\Country|null $billingCountry
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SaleOrder[] $saleOrders
 * @property-read int|null $sale_orders_count
 * @property-read \App\Models\Country|null $shippingCountry
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereBillingCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereBillingCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereBillingState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereBillingStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereBillingZipCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereShippingCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereShippingCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereShippingState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereShippingStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerBranch whereShippingZipCode($value)
 * @mixin \Eloquent
 */
class CustomerBranch extends Model
{
	public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo("App\Models\Customer", 'customer_id');
    }
    public function billingCountry()
    {
    	return $this->belongsTo("App\Models\Country", 'billing_country_id');
    }
    public function shippingCountry()
    {
        return $this->belongsTo("App\Models\Country", 'shipping_country_id');
    }
    public function saleOrders()
    {
        return $this->hasMany('App\Models\SaleOrder', 'customer_branch_id');
    }
}
