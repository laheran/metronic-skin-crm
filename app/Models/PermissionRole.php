<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;

/**
 * App\Models\PermissionRole
 *
 * @property int $permission_id
 * @property int $role_id
 * @method static \Illuminate\Database\Eloquent\Builder|PermissionRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermissionRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PermissionRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|PermissionRole wherePermissionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PermissionRole whereRoleId($value)
 * @mixin \Eloquent
 */
class PermissionRole extends Model
{
	public $timestamps = false;

	public static function getAll()
    {
        $data = Cache::get('gb-permission_roles');
        if (empty($data)) {
            $data = DB::table('permission_roles')->get();
            Cache::put('gb-permission_roles', $data, 30 * 86400);
        }

        return $data;
    }
}
