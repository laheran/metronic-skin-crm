<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProjectMember
 *
 * @property int $id
 * @property int $project_id
 * @property int|null $user_id
 * @property-read \App\Models\Project $project
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectMember whereUserId($value)
 * @mixin \Eloquent
 */
class ProjectMember extends Model
{
    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo("App\Models\Project", 'project_id');
    }
}
