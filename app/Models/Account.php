<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Account
 *
 * @property int $id
 * @property int $account_type_id
 * @property string $name
 * @property string $account_number
 * @property int|null $income_expense_category_id
 * @property int $currency_id
 * @property string $bank_name
 * @property string|null $branch_name
 * @property string|null $branch_city
 * @property string|null $swift_code
 * @property string|null $bank_address
 * @property int $is_default
 * @property int $is_deleted
 * @property-read \App\Models\AccountType $accountType
 * @property-read \App\Models\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deposit[] $deposits
 * @property-read int|null $deposits_count
 * @property-read \App\Models\IncomeExpenseCategory|null $incomeExpenseCategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Account newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Account newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Account query()
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereAccountTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBranchCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBranchName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereIncomeExpenseCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereSwiftCode($value)
 * @mixin \Eloquent
 */
class Account extends Model
{
    public $timestamps = false;

    // Relation Start
    public function deposits()
    {
        return $this->hasMany("App\Models\Deposit", 'account_id');
    }

    public function transactions()
    {
        return $this->hasMany("App\Models\Transaction", 'account_id');
    }

    public function fromBankTransfers()
    {
        return $this->hasMany("App\Models\BankTransfer", 'from_account_id');
    }

    public function toBankTransfers()
    {
        return $this->hasMany("App\Models\BankTransfer", 'to_account_id');
    }
    
    public function accountType()
    {
        return $this->belongsTo("App\Models\AccountType", 'account_type_id');
    }

    public function currency()
    {
       return $this->belongsTo("App\Models\Currency", 'currency_id');
    }

    public function incomeExpenseCategory()
    {
       return $this->belongsTo("App\Models\IncomeExpenseCategory", 'gl_account_id');
    }
    // Relation End

    public function getAccounts()
    {
        $data = DB::select("select ba.*, bat.name as account_type, iec.name as gl_account_name,currency.name as currency_name, SUM(bt.amount) as balance from bank_accounts as ba
                LEFT JOIN bank_transaction as bt
                  ON ba.id = bt.account_no
                LEFT JOIN bank_account_type as bat
                  ON bat.id = ba.account_type_id
                LEFT JOIN income_expense_categories as iec
                  ON iec.id = ba.gl_account_id
                LEFT JOIN currency
                  ON currency.id = ba.currency_id
                where ba.deleted = 0
                GROUP BY bt.account_no");
        return collect($data);
    }
}
