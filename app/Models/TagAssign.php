<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TagAssign
 *
 * @property int $id
 * @property string $tag_type
 * @property int $tag_id
 * @property int $reference_id
 * @property-read \App\Models\Tag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign query()
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign whereReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TagAssign whereTagType($value)
 * @mixin \Eloquent
 */
class TagAssign extends Model
{
	public $timestamps = false;

    public function tag()
    {
        return $this->belongsTo('App\Models\Tag', 'tag_id');
    }
}
