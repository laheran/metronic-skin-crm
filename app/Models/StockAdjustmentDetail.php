<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StockAdjustmentDetail
 *
 * @property int $id
 * @property int $stock_adjustment_id stock_adjustment_id refers to stock_adjustments table id
 * @property int $item_id item_id refers to items table id
 * @property string $description
 * @property string $quantity
 * @property string|null $created_at
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\StockAdjustment $stockAdjustment
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockAdjustmentDetail whereStockAdjustmentId($value)
 * @mixin \Eloquent
 */
class StockAdjustmentDetail extends Model
{
	public $timestamps = false;

	public function stockAdjustment()
	{
		return $this->belongsTo("App\Models\StockAdjustment");
	}

	public function item()
	{
		return $this->belongsTo("App\Models\Item");
	}
}