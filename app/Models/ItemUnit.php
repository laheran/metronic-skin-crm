<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemUnit
 *
 * @property int $id
 * @property string $abbreviation
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Item[] $items
 * @property-read int|null $items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockCategory[] $stockCategory
 * @property-read int|null $stock_category_count
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit query()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemUnit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ItemUnit extends Model
{
    public function stockCategory()
    {
        return $this->hasMany('App\Models\StockCategory', 'item_unit_id');
    }
    
    public function items()
    {
        return $this->hasMany('App\Models\Item', 'item_unit_id');
    }
}
