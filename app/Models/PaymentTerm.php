<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Cache;

/**
 * App\Models\PaymentTerm
 *
 * @property int $id
 * @property string $name
 * @property int $days_before_due
 * @property int $is_default 1 for default;0 for otherwise;
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SaleOrder[] $saleOrders
 * @property-read int|null $sale_orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm whereDaysBeforeDue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentTerm whereName($value)
 * @mixin \Eloquent
 */
class PaymentTerm extends Model
{
	public $timestamps = false;
	
	public function saleOrders()
	{
		return $this->hasMany('App\Models\SaleOrder', 'payment_term_id');
	}

	public function purchaseOrders()
	{
		return $this->hasMany('App\Models\PurchaseOrder', 'payment_term_id');
	}

    public static function getAll()
    {
        $data = Cache::get('gb-payment_terms');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-payment_terms', $data, 30 * 86400);
        }
        return $data;
    }
}
