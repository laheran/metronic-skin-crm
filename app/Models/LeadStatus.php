<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LeadStatus
 *
 * @property int $id
 * @property string $name
 * @property string|null $color
 * @property string $status ('active','inactive')
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read int|null $leads_count
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatus whereStatus($value)
 * @mixin \Eloquent
 */
class LeadStatus extends Model
{
	protected $table = 'lead_statuses';
	public $timestamps = false;

    public function leads()
    {
        return $this->hasMany('App\Models\Lead', 'lead_status_id');
    }
}
