<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PurchaseTax
 *
 * @property int $id
 * @property int $purchase_order_detail_id
 * @property int $tax_type_id
 * @property string $tax_amount
 * @property-read \App\Models\PurchaseOrderDetail $purchaseOrderDetail
 * @property-read \App\Models\TaxType $taxType
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax query()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax wherePurchaseOrderDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseTax whereTaxTypeId($value)
 * @mixin \Eloquent
 */
class PurchaseTax extends Model
{
    public $timestamps = false;

    public function purchaseOrderDetail()
    {
    	return $this->belongsTo('App\Models\PurchaseOrderDetail', 'purchase_order_detail_id');
    }

    public function taxType()
    {
    	return $this->belongsTo('App\Models\TaxType', 'tax_type_id');
    }

	public function getPurchaseTaxes($purch_details_id)
	{
		$taxes = $this->where([ 'purchase_order_detail_id' => $purch_details_id ])->pluck('tax_type_id')->toArray();
	    return json_encode($taxes);
	}

    public function getAllPurchaseTaxesInPercentage($purch_details_id)
    {
        $purchaseOrderDetail = PurchaseOrderDetail::with(['purchaseTaxes:id,purchase_order_detail_id,tax_type_id', 'purchaseOrder:id,tax_type'])->find($purch_details_id);
        $result = [];
        if ($purchaseOrderDetail->purchaseTaxes) {
            foreach ($purchaseOrderDetail->purchaseTaxes as $key => $tax) {
                $result[] = [
                            "id" => $tax->id,
                            "name" => $tax->taxType->name,
                            "rate" => $tax->taxType->tax_rate,
                        ];
            }
        }
        return json_encode($result);
    }
}
