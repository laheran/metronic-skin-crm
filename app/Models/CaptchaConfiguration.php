<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;
use Validator;

/**
 * App\Models\CaptchaConfiguration
 *
 * @property int $id
 * @property string $site_key
 * @property string $secret_key
 * @property string $site_verify_url
 * @property string $plugin_url
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration query()
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration wherePluginUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration whereSecretKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration whereSiteKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CaptchaConfiguration whereSiteVerifyUrl($value)
 * @mixin \Eloquent
 */
class CaptchaConfiguration extends Model
{
    public $timestamps = false;

    /**
     * Store Validation
     * @param  array  $data
     * @return mixed      
     */
    protected function storeValidation($data = []) 
    {
        $validator = Validator::make($data, [
            'site_key' => 'required',
            'secret_key' => 'required',
            'site_verify_url' => 'required',
            'plugin_url' => 'required',
        ]);

        return $validator;
    }

	/**
     * Caching email configuration data
     * @return object $data
    */
    public static function getAll()
    {
        $data = Cache::get('gb-captcha_configurations');
        if (empty($data)) {
            $data = parent::all();
            Cache::put('gb-captcha_configurations', $data, 30 * 86400);
        }
        
        return $data;
    }

    /**
     * Store
     * @param  array  $request
     * @return boolean         
     */
    public function store($request = [])
    {   
        if (parent::updateOrInsert(['id' => 1], $request)) {
            Cache::forget('gb-captcha_configurations');
            return true;
        }

       return false;
    }
}
