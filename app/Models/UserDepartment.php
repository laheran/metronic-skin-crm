<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserDepartment
 *
 * @property int $id
 * @property int|null $user_id user_id refers users table id
 * @property int $department_id department_id refers departments table id
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDepartment whereUserId($value)
 * @mixin \Eloquent
 */
class UserDepartment extends Model
{
	public $timestamps = false;
}
