<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use DB;
use Auth;
use Helpers;
use Session;
use App\Models\Supplier;

class SupplierListDataTable extends DataTable
{
    public function ajax()
    {
        $suppliers = $this->query();
        return datatables()
            ->of($suppliers)
            ->addColumn('supp_name', function ($suppliers) {
                $name = "<a href='" . url("edit-supplier/$suppliers->id") . "'>$suppliers->name</a>";
                return $name;
            })
            ->addColumn('inactive', function ($suppliers) {
                if ($suppliers->is_active == 1) {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-success">Active</div>
                                <!--end::Badges-->
                                </td>';
                } else {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-danger">Inactive</div>
                                <!--end::Badges-->
                                </td>';
                }
                return $status;
            })
            ->addColumn('address', function ($suppliers) {
                $address = '<p>' . (isset($suppliers->street) ? $suppliers->street : '') . '' . (isset($suppliers->city) ? ', ' . $suppliers->city : '') . (isset($suppliers->state) ? ', ' . $suppliers->state : '') . '' . (isset($suppliers->zipcode) ? ', ' . $suppliers->zipcode : '') . '' . (isset($suppliers->countryName) ? ', ' . $suppliers->countryName : '') . '</p>';
                return $address;

            })
            ->addColumn('created_at', function ($suppliers) {

//                $startDate = timeZoneformatDate($suppliers->created_at);
//                $startTime = timeZonegetTime($suppliers->created_at);
//                return $startDate . '<br>' . $startTime;
                return $suppliers->created_at;
            })
            ->addColumn('action', function ($suppliers) {
                $action = '<a class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click"
                                data-kt-menu-placement="bottom-end">
                                Actions
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-180.000000) translate(-12.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                </span>
                                </a>
                                <!--begin::Menu 3-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-150px py-3"
                                     data-kt-menu="true">
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url("edit-supplier/$suppliers->id") . '" class="menu-link px-3">Edit</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url("supplier/show/" . $suppliers->id) . '" class="menu-link px-3">Details</a>
                                        </div>
                                        <!--end::Menu item-->
                                </div>
                                <!--end::Menu 3-->';
                return $action;
            })
            ->rawColumns(['supp_name', 'inactive', 'address', 'created_at', 'action'])
            ->make(true);
    }

    public function query()
    {
//        $supplier = isset($_GET['supplier']) ? $_GET['supplier'] : null;
//        $suppliers = Supplier::select();
//        if (!empty($supplier) && $supplier == "inactive") {
//            $suppliers->where('is_active', 0);
//        } else if (!empty($supplier) && $supplier == "total") {
//            $suppliers;
//        } else {
//            $suppliers->where('is_active', 1);
//        }
//
//        return $this->applyScopes($suppliers);
        return Supplier::all();
    }

    public function html()
    {
        return $this->builder()
            ->addColumn(['data' => 'id', 'name' => 'id', "visible" => false])
            ->addColumn(['data' => 'supp_name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => __('Email')])
            ->addColumn(['data' => 'contact', 'name' => 'contact', 'title' => __('Phone')])
            ->addColumn(['data' => 'address', 'name' => 'address', 'title' => __('Address')])
            ->addColumn(['data' => 'inactive', 'name' => 'is_active', 'title' => __('Status'), 'orderable' => false])
            ->addColumn(['data' => 'created_at', 'name' => 'suppliers.created_at', 'title' => __('Created at')])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => __('Action')])
            ->parameters([
                'pageLength' => $this->row_per_page,
                'language' => [
                    'url' => url('/resources/lang/' . config('app.locale') . '.json'),
                ],
            ]);
    }

    protected function getColumns()
    {
        return [
            'id',
            'created_at',
            'updated_at',
        ];
    }

    protected function filename()
    {
        return 'customers_' . time();
    }
}
