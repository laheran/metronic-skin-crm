<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Models\{
    Customer
};
use Auth;
use DB;
use Helpers;
use Session;

class CustomerListDataTable extends DataTable
{
    public function ajax()
    {
        $customers = $this->query();
        return datatables()
            ->of($customers)
            ->addColumn('name', function ($customers) {
                $customer = $customers->name;
                return $customer;
            })
            ->addColumn('created_at', function ($customers) {
//                return $customers->created_at->diffForHumans();
                return $customers->created_at;
//                return \Carbon\Carbon::parse($customers->created_at)->diffForHumans();
//                return timeZoneformatDate($customers->created_at) . '<br>' . timeZonegetTime($customers->created_at);
            })
            ->addColumn('inactive', function ($customers) {
                if ($customers->is_active == 1) {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-success">Active</div>
                                <!--end::Badges-->
                                </td>';
                } else {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-danger">Inactive</div>
                                <!--end::Badges-->
                                </td>';
                }
                return $status;
            })
            ->addColumn('action', function ($customers) {
                if (Helpers::has_permission(Auth::user()->id, 'edit_team_member')) {
                    $action = '<a class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click"
                                data-kt-menu-placement="bottom-end">
                                Actions
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-180.000000) translate(-12.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                </span>
                                </a>
                                <!--begin::Menu 3-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-150px py-3"
                                     data-kt-menu="true">
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url('customer/edit/' . $customers->id) . '" class="menu-link px-3">Edit</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url("customer/adminlogin/" . $customers->id) . '" class="menu-link px-3">Connect</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url("customer/show/" . $customers->id) . '" class="menu-link px-3">Details</a>
                                        </div>
                                        <!--end::Menu item-->
                                </div>
                                <!--end::Menu 3-->';
                    $actions = '<a href="' . url('customer/edit/' . $customers->id) . '" class="btn btn-icon btn-active-light-success w-30px h-30px"
                           data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" data-bs-placement="top" title="Edit">
                            <span class="svg-icon svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.25" x="3" y="21" width="18" height="2" rx="1" fill="#191213"></rect>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M4.08576 11.5L3.58579 12C3.21071 12.375 3 12.8838 3 13.4142V17C3 18.1045 3.89543 19 5 19H8.58579C9.11622 19 9.62493 18.7893 10 18.4142L10.5 17.9142L4.08576 11.5Z"
                              fill="#121319"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 10.0858L11.5858 4L18 10.4142L11.9142 16.5L5.5 10.0858Z"
                              fill="#121319"></path>
                        <path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd"
                              d="M18.1214 1.70705C16.9498 0.535476 15.0503 0.535476 13.8787 1.70705L13 2.58576L19.4142 8.99997L20.2929 8.12126C21.4645 6.94969 21.4645 5.0502 20.2929 3.87862L18.1214 1.70705Z"
                              fill="#191213"></path>
                        </svg>
                            </span>
                        </a>  
                        
                        &nbsp;
                
                               <a href="' . url("customer/adminlogin/" . $customers->id) . '" class="btn btn-icon btn-active-light-success w-30px h-30px"
                       data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" data-bs-placement="top" title="' . __('Login as') . '">
                        <span class="svg-icon svg-icon-2x">
                    <!--begin::Svg Icon | path: assets/media/icons/duotune/coding/cod007.svg-->
                    <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path opacity="0.3" d="M18.4 5.59998C18.7766 5.9772 18.9881 6.48846 18.9881 7.02148C18.9881 7.55451 18.7766 8.06577 18.4 8.44299L14.843 12C14.466 12.377 13.9547 12.5887 13.4215 12.5887C12.8883 12.5887 12.377 12.377 12 12C11.623 11.623 11.4112 11.1117 11.4112 10.5785C11.4112 10.0453 11.623 9.53399 12 9.15698L15.553 5.604C15.9302 5.22741 16.4415 5.01587 16.9745 5.01587C17.5075 5.01587 18.0188 5.22741 18.396 5.604L18.4 5.59998ZM20.528 3.47205C20.0614 3.00535 19.5074 2.63503 18.8977 2.38245C18.288 2.12987 17.6344 1.99988 16.9745 1.99988C16.3145 1.99988 15.661 2.12987 15.0513 2.38245C14.4416 2.63503 13.8876 3.00535 13.421 3.47205L9.86801 7.02502C9.40136 7.49168 9.03118 8.04568 8.77863 8.6554C8.52608 9.26511 8.39609 9.91855 8.39609 10.5785C8.39609 11.2384 8.52608 11.8919 8.77863 12.5016C9.03118 13.1113 9.40136 13.6653 9.86801 14.132C10.3347 14.5986 10.8886 14.9688 11.4984 15.2213C12.1081 15.4739 12.7616 15.6039 13.4215 15.6039C14.0815 15.6039 14.7349 15.4739 15.3446 15.2213C15.9543 14.9688 16.5084 14.5986 16.975 14.132L20.528 10.579C20.9947 10.1124 21.3649 9.55844 21.6175 8.94873C21.8701 8.33902 22.0001 7.68547 22.0001 7.02551C22.0001 6.36555 21.8701 5.71201 21.6175 5.10229C21.3649 4.49258 20.9947 3.93867 20.528 3.47205Z" fill="black"/>
                    <path d="M14.132 9.86804C13.6421 9.37931 13.0561 8.99749 12.411 8.74695L12 9.15698C11.6234 9.53421 11.4119 10.0455 11.4119 10.5785C11.4119 11.1115 11.6234 11.6228 12 12C12.3766 12.3772 12.5881 12.8885 12.5881 13.4215C12.5881 13.9545 12.3766 14.4658 12 14.843L8.44699 18.396C8.06999 18.773 7.55868 18.9849 7.02551 18.9849C6.49235 18.9849 5.98101 18.773 5.604 18.396C5.227 18.019 5.0152 17.5077 5.0152 16.9745C5.0152 16.4413 5.227 15.93 5.604 15.553L8.74701 12.411C8.28705 11.233 8.28705 9.92498 8.74701 8.74695C8.10159 8.99737 7.5152 9.37919 7.02499 9.86804L3.47198 13.421C2.52954 14.3635 2.00009 15.6417 2.00009 16.9745C2.00009 18.3073 2.52957 19.5855 3.47202 20.528C4.41446 21.4704 5.69269 21.9999 7.02551 21.9999C8.35833 21.9999 9.63656 21.4704 10.579 20.528L14.132 16.975C14.5987 16.5084 14.9689 15.9544 15.2215 15.3447C15.4741 14.735 15.6041 14.0815 15.6041 13.4215C15.6041 12.7615 15.4741 12.108 15.2215 11.4983C14.9689 10.8886 14.5987 10.3347 14.132 9.86804Z" fill="black"/>
                    </svg></span>
                    <!--end::Svg Icon-->
                        </span>
                    </a>';
                }
                return $action;
            })
            ->addColumn('address', function ($customers) {
                $address = [];
                $implodedAddress = '';
                if (!empty($customers->customerBranch->billing_street)) {
                    $address[] = $customers->customerBranch->billing_street;
                }
                if (!empty($customers->customerBranch->billing_city)) {
                    $address[] = $customers->customerBranch->billing_city;
                }
                if (!empty($customers->customerBranch->billing_state)) {
                    $address[] = $customers->customerBranch->billing_state;
                }
                if (!empty($customers->customerBranch->billing_zip_code)) {
                    $address[] = $customers->customerBranch->billing_zip_code;
                }
                if (!empty($customers->customerBranch->billingCountry)) {
                    $address[] = $customers->customerBranch->billingCountry->name;
                }
                if (!empty($address)) {
                    $implodedAddress = implode(', ', $address);
                }

                return $implodedAddress;
            })
            ->rawColumns(['name', 'created_at', 'inactive', 'address', 'action'])
            ->make(true);
    }

    public function query()
    {
//        $customer = isset($_GET['customer']) ? $_GET['customer'] : null;
//        $customer = Customer::all();
//        $customers = Customer::with('customerBranch')->select();
//        $customers = Customer::all();
//        if (!empty($customer) && $customer == "inactive") {
//            $customers->where('is_active', 0);
//        } else if (!empty($customer) && $customer == "total") {
//            $customers;
//        } else {
//            $customers->where('is_active', 1);
//        }
//        return $this->applyScopes($customers);
        return Customer::all();
    }

    public function html()
    {
        return $this->builder()
            ->addColumn(['data' => 'id', 'name' => 'id', "visible" => false])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => __('Name')])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => __('Email')])
            ->addColumn(['data' => 'phone', 'name' => 'phone', 'title' => __('Phone')])
            ->addColumn(['data' => 'address', 'name' => 'address', 'title' => __('Address')])
            ->addColumn(['data' => 'inactive', 'name' => 'inactive', 'title' => __('Status')])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => __('Created At')])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => __('Action')])
            ->parameters([
                'pageLength' => $this->row_per_page,
                'language' => [
                    'url' => url('/resources/lang/' . config('app.locale') . '.json'),
                ],
            ]);
    }

    protected function getColumns()
    {
        return [
            'id',
            'created_at',
            'updated_at',
        ];
    }

    protected function filename()
    {
        return 'customers_' . time();
    }
}
