<?php
namespace App\DataTables;
use Yajra\DataTables\Services\DataTable;
use App\Models\{
    User,
    File
};
use DB;
use Auth;
use Helpers;
use Session;

class UserListDataTable extends DataTable
{
    public function ajax()
    {
        $users = $this->query();
        return datatables()
            ->of($users)
            ->addColumn('action', function ($users) {
                $delete = $edit = '';
//                if ($users->id != Auth::user()->id && $users->id != 1) {
//                    $edit = Helpers::has_permission(Auth::user()->id, 'edit_team_member') ? '<a href="'. url('user/team-member-profile/'. $users->id) .'" class="btn btn-xs btn-primary"><i class="feather icon-edit"></i></a>&nbsp;':'';
//                    if (Helpers::has_permission(Auth::user()->id, 'delete_team_member')) {
//                        $delete= '<form method="post" action="'. url('delete-user/'. $users->id) .'" id="delete-user-'. $users->id .'" accept-charset="UTF-8" class="display_inline">
//                        '. csrf_field() .'
//                        <button title="' . __('Delete') . '" class="btn btn-xs btn-danger" type="button" data-id='.$users->id.' data-label="Delete" data-toggle="modal" data-target="#confirmDelete" data-title="' . __('Delete user') . '" data-message="' . __('Are you sure to delete this user?') . '">
//                        <i class="feather icon-trash-2"></i>
//                        </button>
//                        </form>';
//                    }
//                };

                $action = '<a class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click"
                                data-kt-menu-placement="bottom-end">
                                Actions
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-180.000000) translate(-12.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                </span>
                                </a>
                                <!--begin::Menu 3-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-150px py-3"
                                     data-kt-menu="true">
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url('user/team-member-profile/'. $users->id) . '" class="menu-link px-3">Edit</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="' . url("user/show/" . $users->id) . '" class="menu-link px-3">Details</a>
                                        </div>
                                        <!--end::Menu item-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                        <form method="post" action="'. url('delete-user/'. $users->id) .'" id="delete-user-'. $users->id .'" accept-charset="UTF-8" class="display_inline">
                                            '. csrf_field() .'
                                            <button title="' . __('Delete') . '" 
                                            class="menu-link px-3" type="button" 
                                            data-id='.$users->id.' data-label="Delete" data-toggle="modal" 
                                            data-target="#confirmDelete" data-title="' . __('Delete user') . '" data-message="' . __('Are you sure to delete this user?') . '">
                                            <i class="feather icon-trash-2"></i>
                                            </button>
                                              </form>
                                        </div>
                                        <!--end::Menu item-->
                                </div>
                                <!--end::Menu 3-->';
//                <a href="' . url("user/show/" . $users->id) . '" class="menu-link px-3">Delete</a>

//                return $edit . $delete;
                return $action;
            })
//            ->addColumn('img', function ($users) {
//                if (isset($users->avatarFile->file_name)  && !empty($users->avatarFile->file_name)) {
//                    if (file_exists('public/uploads/user/thumbnail/' . $users->avatarFile->file_name)) {
//                        $img = '<img src="'. url("public/uploads/user/thumbnail/". $users->avatarFile->file_name) .'" alt="" width="50" height="50">';
//                    } else {
//                        $img = '<img src="'. url("public/dist/img/avatar.jpg") .'" alt="" width="50" height="50">';
//                    }
//                } else {
//                        $img = '<img src="'. url("public/dist/img/avatar.jpg") .'" alt="" width="50" height="50">';
//                }
//                return $img;
//            })
            ->addColumn('status', function ($users) {
                $status = '';
                if ($users->is_active == 1) {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-success">Active</div>
                                <!--end::Badges-->
                                </td>';
                } else {
                    $status = '<td class="text-end pe-0" data-order="Published">
                                <!--begin::Badges-->
                                <div class="badge badge-light-danger">Inactive</div>
                                <!--end::Badges-->
                                </td>';
                }
                return $status;
            })
            ->addColumn('full_name', function ($users) {
                if (Helpers::has_permission(Auth::user()->id, 'edit_team_member')) {
                    return '<a href="'. url('user/team-member-profile/'. $users->id) .'">'. $users->full_name .'</a><br>';
                }
                return  $users->full_name;
            })
            ->addColumn('email', function ($users) {
                if (mb_strlen($users->email) > 20) {
                    $user_mail = mb_substr($users->email, 0, 20) . "..";
                } else {
                    $user_mail = $users->email;
                }
                $full_mail = $users->email;
                return mb_strlen($full_mail) > 20 ? '<span data-toggle="tooltip" data-placement="right" data-original-title="'. $full_mail .'">'. $user_mail .'</span>' : $user_mail;
            })
            ->addColumn('created_at', function ($users) {
//                return timeZoneformatDate($users->created_at) .'<br>'. timeZonegetTime($users->created_at);
                return $users->created_at;
            })
            ->addColumn('role', function ($users) {
                return !empty($users->role) ? $users->role->display_name : '';
            })
            ->rawColumns(['action', 'img', 'full_name', 'email', 'status', 'created_at', 'role'])
            ->make(true);
    }

    public function query()
    {
//        $user = isset($_GET['user']) ? $_GET['user'] : null;
//        $users = User::with(['role', 'avatarFile:object_id,object_type,file_name'])->select();
//        if (!empty($user) && $user == "inactive") {
//            $users->where('is_active', 0);
//        } else if (!empty($user) && $user == "total") {
//            $users;
//        } else {
//            $users->where('is_active', 1);
//        }
//
//        return $this->applyScopes($users);
        return User::all();
    }

    public function html()
    {
        return $this->builder()
            ->addColumn(['data' => 'id', 'name' => 'id', "visible" => false])
//            ->addColumn(['data' => 'img', 'name' => 'img', 'title' => __('Picture'), 'orderable' => false, 'searchable' => false])
            ->addColumn(['data' => 'full_name', 'name' => 'users.full_name', 'title' => __('Name')])
            ->addColumn(['data' => 'email', 'name' => 'users.email', 'title' => __('Email')])
            ->addColumn(['data' => 'role', 'name' => 'role.display_name', 'title' => __('Role')])
            ->addColumn(['data' => 'phone', 'name' => 'phone', 'title' => __('Phone')])
             ->addColumn(['data'=> 'status', 'name' => 'status', 'title' => __('Status'), 'orderable' => false, 'searchable' => false])
             ->addColumn(['data' => 'created_at', 'name' => 'users.created_at', 'title' => __('Created at')])
            ->addColumn(['data'=> 'action', 'name' => 'action', 'title' => __('Action'), 'orderable' => false, 'searchable' => false])
            ->parameters([
                'pageLength' => $this->row_per_page,
                'language' => [
                        'url' => url('/resources/lang/'.config('app.locale').'.json'),
                    ],
//                'order' => [0, 'DESC']
            ]);
    }

    protected function getColumns()
    {
        return [
            'id',
            'created_at',
            'updated_at',
        ];
    }

    protected function filename()
    {
        return 'customers_' . time();
    }
}
